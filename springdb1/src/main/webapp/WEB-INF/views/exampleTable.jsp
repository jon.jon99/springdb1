<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>Example Page</h1>
				<%@ include file="/WEB-INF/layouts/message.jsp" %>
				${message}		
				<!-- Table tag and table header tags here -->
				<c:if test="${empty exampleList}">
					<tr><td colspan="3">No Data Found</td></tr>
				</c:if>
				<c:if test="${not empty exampleList}">
					<c:forEach var="example" items="${exampleList}">
						<tr>
					    	<td>
					    		<c:out value="${example.id}"/>
					    	</td>
							<td>
					    		<c:out value="${example.firstName}"/>
					    	</td>
					    	<td>
					    		<c:out value="${example.lastName}"/>
					    	</td>
					    </tr> 
					</c:forEach>
				</c:if>
			</div>
		</div>
	</div>
</body>
</html>